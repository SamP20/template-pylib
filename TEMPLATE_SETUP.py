import os
import re
from mako.template import Template

def initialize(info):
    substitute_file("README.md", info)
    substitute_file("setup.py", info)
    module_name = info["name"].lower().replace("-", "_")
    module_name = re.sub("[^a-z0-9_]+", "", module_name)
    module_path = os.path.join("src", module_name)
    os.makedirs(module_path)
    open(os.path.join(module_path, "__init__.py"), "a").close()


def substitute_file(name, info):
    template = Template(filename=name)
    with open(name, "w") as fh:
        fh.write(template.render(**info))